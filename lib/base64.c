#include <stdio.h>
#include "../lib/base64.h"

size_t base64_decode_len(size_t length, size_t pads) {
    // Pads refer to number of padding characters '='
    // Can only be either 0, 1, or 2; length must
    // be multiple of 4
    return ((3*(length - pads)) >> 2);
}

size_t base64_encode_len(size_t length) {
    return ((length/3) + (length%3 != 0)) << 2;
}

int base64_decode(const uint8_t* b64, uint8_t* plain, size_t size) {
    // Format does not match base64 encoding
    if (size%4 != 0 || !b64 || !plain) return -1;
    // Decoding
    int i; // Need to declare it outside the loop to deal with B64_PADding
    // Leave the last four characters and decode case by case
    for (i = 0; i < size-4; i += 4) {
        *plain++ = (B64_TO_INT[b64[i]] << 2) | (B64_TO_INT[b64[i+1]] >> 4);
        *plain++ = (B64_TO_INT[b64[i+1]] << 4) | (B64_TO_INT[b64[i+2]] >> 2);
        *plain++ = (B64_TO_INT[b64[i+2]] << 6) | B64_TO_INT[b64[i+3]];
    }
    // At least one more character
    *plain++ = (B64_TO_INT[b64[i]] << 2) | (B64_TO_INT[b64[i+1]] >> 4);
    if (b64[i+2] != B64_PAD) {
        *plain++ = (B64_TO_INT[b64[i+1]] << 4) | (B64_TO_INT[b64[i+2]] >> 2);
    }
    if (b64[i+3] != B64_PAD) { // One padding character
        *plain++ = (B64_TO_INT[b64[i+2]] << 6) | B64_TO_INT[b64[i+1]];
    }
    // Null terminating character
    *plain = '\0';
    // Exit successful
    return 0;
}

int base64_encode(const uint8_t* plain, uint8_t* b64, size_t size) {
    if (!plain || !b64 || plain[0] == '\0') return -1;
    int i;
    for (i = 0; i+2 < size; i += 3) {
        *b64++ = MIME[plain[i] >> 2];
        *b64++ = MIME[((plain[i] & 0x03) << 4) | (plain[i+1] >> 4)];
        *b64++ = MIME[((plain[i+1] & 0x0F) << 2) | (plain[i+2] >> 6)];
        *b64++ = MIME[plain[i+2] & 0x3F];
    }
    // Padding, if necessary
    if (i < size) {
        *b64++ = MIME[plain[i] >> 2];
        if (i+2 == size) {
            *b64++ = MIME[((plain[i] & 0x03) << 4) | (plain[i+1] >> 4)];
            *b64++ = MIME[(plain[i+1] & 0x0F) << 2];
        } else {
            *b64++ = MIME[((plain[i] & 0x03) << 4)];
            *b64++ = B64_PAD;
        }
            *b64++ = B64_PAD;
    }
    // Null terminating character
    *b64 = '\0';
    // Exit successful
    return 0;
}
