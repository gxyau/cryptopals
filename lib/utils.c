#include "constants.h"
#include "utils.h"

static const uint8_t* hex_codes = "0123456789ABCDEF";

int str_xor(const uint8_t* str1, const uint8_t* str2, uint8_t* xor, size_t size) {
    // Asserting both strings are of equal length
    if (strlen(str1) != size || strlen(str1) != strlen(str2)) return -1;
    // Computing xor bitwise
    for (int i = 0; i < size; ++i) {
        *xor++ = HEX[B16_TO_INT[str1[i]] ^ B16_TO_INT[str2[i]]];
    }
    // Null terminating character
    *xor = '\0';
    // Exit successful
    return 0;
}

double score(const uint8_t* str, const uint8_t x, size_t size) {
    double total = 0.0;
    for (int i = 0; i < size; ++i) {
        char c = str[i] ^ x;
        if ('A' <= c && c <= 'Z') {
            total += ENG_ALPHA_FREQ[c - 'A'];
        } else if ('a' <= c && c <= 'z') {
            total += ENG_ALPHA_FREQ[c - 'a'];
        } else if (c == ' ') {
            total += 0.13;
        }
    }
    return total;
}


int bit_xor(const uint8_t* str, const uint8_t bit, uint8_t* xor, size_t size) {
    if ( !str || !xor || strlen(str) != size) return -1;
    for (int i = 0; i < size; ++i) {
        xor[i] = str[i] ^ bit;
    }
    xor[size] = '\0';
    // Exit successful
    return 0;
}

double guess_xor(const uint8_t* enc, uint8_t* xor, uint8_t* candidate, size_t size) {
    if (!enc || !xor) return -1;
    double current_score, max = -1.0;
    for (int i = 0; i < 256; ++i) {
        current_score = score(enc, i, size);
        if (current_score > max) {
            *candidate = i;
            max = current_score;
        }
    }
    printf("Best candidate: %d with score %f\n", *candidate, max);
    
    // Building xor string
    int status = bit_xor(enc, *candidate, xor, size);
    if (status == -1) return EXIT_FAILURE;

    // Return best score
    return max;
}

int repeated_xor(const uint8_t* plain, const uint8_t* key, uint8_t* enc, size_t str_size, size_t key_size) {
    for (int i = 0; i < str_size; ++i) {
        enc[i] = plain[i] ^ key[i%key_size];
    }
    enc[str_size] = '\0';
    // Exit succesful
    return 0;
}

int hamming(const uint8_t* s, const uint8_t* t, size_t size) {
    // Counting distance bytewise
    assert(t - s == size);
    int dist = 0;
    for (int i = 0; i < size; ++i) {
        dist += __builtin_popcount(s[i] ^ t[i]);
    }

    // Returning the distance
    return dist;
}

int guess_key_size(const uint8_t* enc, size_t len, const size_t lo, const size_t hi) {
    // Error in input
    if (!enc || lo > hi || strlen(enc) < 1) return -1;
    size_t bestlen = -1;
    double bestscore = DBL_MAX;

    for (size_t sz = lo; sz <= hi; ++sz) {
        double score = 0;
        int nblocks = len / sz;
        for (int i = 0; i < nblocks-1; i+=2)
            score += (double) hamming(&enc[sz*i], &enc[sz * (i + 1)], sz) / sz;
        score /= nblocks;
        if (score < bestscore) {
            bestscore = score;
            bestlen =  sz;
        }
        printf("length: %d, score: %lf\n", sz, score);
     }
     return bestlen;
}

int juxtapose(const uint8_t* enc, const int pos, const int keylen, uint8_t* block, size_t enc_len) {
    if (!enc || !keylen || !block) return -1;

    // Creating juxtaposed blocks
    int index = pos;
    while (index < enc_len) {
        *block++ = enc[index];
        index += keylen;
    }

    // Exit successful
    return 0;
}
