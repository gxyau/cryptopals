#include "base16.h"
#include <stdio.h>

size_t base16_decode_len(const size_t length)  {return (length >> 1);}

size_t base16_encode_len(const size_t length) {return (length << 1);}

int base16_decode(const uint8_t* b16, uint8_t* plain, size_t size) {
    // Error exit status
    if (size%2 != 0 || !b16 || !plain) return -1;
    // Decoding
    for (int i = 0; i+1 < size; i += 2) {
        *plain++ = (B16_TO_INT[b16[i]] << 4) | B16_TO_INT[b16[i+1]];
    }
    // Null terminating string
    *plain = '\0';
    // Exit successful
    return 0;
}

int base16_encode(const uint8_t* plain, uint8_t* b16, size_t size) {
    // If plain text is not given or encoding string is nto given
    if (!plain || !b16) return -1;
    // Encoding
    int index = 0;
    for (int i = 0; i < size; ++i) {
      *b16++ = HEX[plain[i] >> 4];
      *b16++ = HEX[plain[i] & 0x0F];
    }
    // Null terminating string
    b16[index] = '\0';
    // Exit successful
    return 0;
}
