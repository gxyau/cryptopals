#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <stddef.h>
#include <stdint.h>
#include <string.h>

extern const uint8_t B64_PAD;

extern const uint8_t HEX[16];

extern const uint8_t MIME[64];

extern const uint8_t B16_TO_INT[128];

extern const uint8_t B64_TO_INT[128];

extern const double ENG_ALPHA_FREQ[26];

#endif
