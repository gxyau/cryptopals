#ifndef BASE_64_H
#define BASE_64_H

#include "constants.h"
#include <stddef.h>
#include <stdint.h>
#include <string.h>

size_t base64_decode_len(const size_t, const size_t);
size_t base64_encode_len(const size_t);

int base64_decode(const uint8_t*, uint8_t*, size_t);
int base64_encode(const uint8_t*, uint8_t*, size_t);

#endif
