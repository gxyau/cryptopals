#ifndef BASE_16_H
#define BASE_16_H

#include "constants.h"
#include <stddef.h>
#include <stdint.h>
#include <string.h>

size_t base16_decode_len(const size_t);
size_t base16_encode_len(const size_t);

int base16_decode(const uint8_t*, uint8_t*, size_t);
int base16_encode(const uint8_t*, uint8_t*, size_t);

#endif
