#ifndef UTILS_H
#define UTILS_H

#include <assert.h>
#include "constants.h"
#include <ctype.h>
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int str_xor(const uint8_t*, const uint8_t*, uint8_t*, size_t);

double score(const uint8_t*, const uint8_t, size_t);

int bit_xor(const uint8_t*, const uint8_t, uint8_t*, size_t);

double guess_xor(const uint8_t*, uint8_t*, uint8_t*, size_t);

int repeated_xor(const uint8_t*, const uint8_t*, uint8_t*, size_t, size_t);

int hamming(const uint8_t*, const uint8_t*, size_t);

int guess_key_size(const uint8_t*, size_t, const size_t, const size_t);

int juxtapose(const uint8_t*, const int, const int, uint8_t*, size_t);

#endif
