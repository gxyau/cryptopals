from set1 import *
from random import randrange, choice

def xor(*args):
    if not args:
        return b""
    s = args[0]
    for t in args[1:]:
        arr = []
        for i in range(max(len(s), len(t))):
            arr.append(s[i%len(s)] ^ t[i%len(t)])
        s = bytes(arr)
    return s

def pad(text, length = 16):
    n = length - (len(text)%length)
    return text + bytes([n]) * n

def unpad(text):
    return text[:-text[-1]]

def cbc_encrypt(key, plain, iv):
    assert len(iv) == 16
    if ( len(plain) % 16 != 0):
        plain = pad(plain, 16)
    blocks = [iv]
    for block in make_blocks(plain, 16):
        blocks.append(ecb_encrypt(key, xor(block, blocks[-1])))
    return b''.join(blocks[1:])

def cbc_decrypt(key, cipher, iv):
    blocks = [iv] + make_blocks(cipher, 16)
    arr = []
    for ind, block in enumerate(blocks):
        if not ind:
            continue
        arr.append(xor(blocks[ind - 1], ecb_decrypt(key, block)))
    return b"".join(arr)

def keygen(length = 16):
    return bytes(randrange(0,256) for _ in range(length))

def encryption_oracle(plain):
    def ecb_enc(k, m):
        return ecb_encryption(k, pad(m))

    def cbc_enc(k, m):
        return cbc_encrypt(k, m, keygen())

    key = keygen()
    c   = choice([0,1])
    enc = [ecb_enc, cbc_enc][choice]

    def oracle(plain):
        return enc(key, randrange(5,11) + plain + randrange(5,11))
    
    return c, oracle(plain)

def identify_encryption_mode(oracle):
    cipher = oracle(512 * b'A')
    blocks = make_blocks(cipher, 16)
    return (len(blocks) - len(set(blocks))) < 2


def simple_ecb_oracle(random_prefix = False):
    key = keygen()
    suffix = b64decode(b"Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK")
    prefix = keygen(randrange(256)) if random_prefix else b''
    def oracle(plain):
        return ecb_encrypt(key, pad(prefix+plain+suffix))

    return oracle

def attack_simple_ecb_oracle(oracle):
    def determine_len(max_len = 100):
        original_len = len(oracle(b''))

        # Determine block length
        for i in range(max_len):
            payload = i * b'A'
            if len(oracle(payload)) != original_len:
                block_len = len(oracle(payload)) - original_len
                break

        # Determine message length
        payload = (2*block_len) * b'A'
        prefix_len = 0
        message_len = -1
        while message_len < 0:
            enc = oracle(payload)
            for i in range(0, len(enc) - block_len, block_len):
                if enc[i:i+block_len] == enc[i+block_len:i+2*block_len]:
                    prefix_len = i + 2*block_len - len(payload)
                    message_len = len(enc) - i - 2*block_len
                    break
            else:
                payload += b'A'
        return block_len, message_len, prefix_len

    # Step 1: Determine block size
    BLOCK_LEN, MESSAGE_LEN, PREFIX_LEN = determine_len()
    assert (BLOCK_LEN > 0), "Unable to determine block length, try increasing the max_len"

    # Step 2: Determine encryption mode
    MODE = {0 : "ECB", 1 : "CBC"}
    encryption_mode = identify_encryption_mode(oracle)
    print(f"The encryption mode is: {MODE[encryption_mode]}")
    assert encryption_mode == 0

    # Step 3: Feeding payload to oracle and remember outcomes
    payload_encryptions = []
    BUFFER_LEN = 0 if PREFIX_LEN%BLOCK_LEN == 0 else BLOCK_LEN - (PREFIX_LEN%BLOCK_LEN)
    OFFSET_BLOCKS = (PREFIX_LEN + BUFFER_LEN)//BLOCK_LEN
    for i in range(BLOCK_LEN):
        blocks = make_blocks(oracle((BUFFER_LEN + i) * b'A'), BLOCK_LEN)[OFFSET_BLOCKS:]
        payload_encryptions += [blocks]

    # Step 4: Brute force
    message = (BUFFER_LEN + BLOCK_LEN) * b'A'
    offset  = 0
    index = PREFIX_LEN+BUFFER_LEN
    while offset < MESSAGE_LEN:
        # Iterating through each encryption and getting corresponding block
        encryption = payload_encryptions[BLOCK_LEN - 1 - (offset % BLOCK_LEN)]
        enc = encryption[offset//BLOCK_LEN]
        prefix = message[offset+1 : offset+BLOCK_LEN+BUFFER_LEN]
        
        for i in range(256):
            payload = prefix + bytes([i])
            target = make_blocks(oracle(payload), BLOCK_LEN)[index//BLOCK_LEN]
            if make_blocks(oracle(payload), BLOCK_LEN)[index//BLOCK_LEN] == enc:
                message += bytes([payload[-1]])
        # Update variables
        offset += 1

    return message[BUFFER_LEN+BLOCK_LEN:].decode("utf-8")

def kv_decode(s):
    return {t.split('=') for t in s.strip().split('&')}

def replace_meta_char(x):
    return x.replace('&','').replace('=','')

def kv_encode(D):
    return '&'.join(k + '=' + v for k,v in D.items())

def profile_for(email):
    return kv_encode({"email": replace_meta_char(email), "uid": "10", "role": "user"})

def forge_admin():
    KEY = keygen()
    # email=me@bar.comadminXXXXXXXXXXXYYY&uid=10&role=userXXXXXXXXXXXXXX
    # 0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef
    email  = "fu@bar.com" + "admin" + "\x0b" * 11 + "Y" * 3
    forged = ecb_encrypt(KEY, pad(profile_for(email).encode()))
    return KEY, forged[:48] + forged[16:32]

class PaddingError(Exception):
    pass

def validate_padding(s):
    if not(1 <= s[-1] <= 16):
        raise PaddingError()
    for i in range(s[-1]):
        if s[-(i+1)] != s[-1]:
            raise PaddingError()

class CBCBitflippingAttack():
    def __init__(self):
        self.__KEY = keygen()
        self.__IV  = keygen()

    def generate_query(self, s):
        print(f"s: {s}")
        t = b''.join([b''.join(u.split(b'=')) for u in s.strip().split(b';')])
        print(f"Clean text: {t}")
        prefix = b"comment1=cooking%20MCs;userdata="
        suffix = b";comment2=%20like%20a%20pound%20of%20bacon"
        print(f"string: {prefix+t+suffix}")
        return cbc_encrypt(self.__KEY, pad(prefix+t+suffix), self.__IV)

    def verify_admin(self, s):
        t = cbc_decrypt(self.__KEY, s, self.__IV)
        print(f"Decrypted string: {t}")
        collection = [u.split(b'=') for u in t.strip().split(b';')]
        print(collection)
        return [b'admin', b'true'] in collection

# bitflip(c, b"\0" * 16, b";admin=true;x=", 48)
# bitflip(c, known, target, offset)

def bit_flip(cipher, known, target):
    return xor(cipher, known, target)


if __name__ == "__main__":
    print("======================================== Cryptopals Set 2 ========================================")

    print("==================== Challenge 9 ====================")
    print(pad(b"YELLOW SUBMARINE", 20))
    print("\n")

    print("==================== Challenge 10 ====================")
    plain  = b"A quick brown for jumps over the lazy dog"
    key    = b"DISCONNECTEDNESS"
    IV     = b"SIGNALPROCESSING"
    cipher = cbc_encrypt(key, plain, IV)
    print(f"Plain length: {len(plain)}, cipher length: {len(cipher)}")
    print(cbc_decrypt(key, cipher, IV))
    print("\n")

    print("==================== Challenge 12 ====================")
    plain = attack_simple_ecb_oracle(simple_ecb_oracle())
    print(f"Decoded message:\n{plain}")
    print("\n")

    print("==================== Challenge 13 ====================")
    key, enc = forge_admin()
    plain = ecb_decrypt(key, enc)
    print(f"Decrypted forged string: {plain}")
    print("\n")

    print("==================== Challenge 14 ====================")
    plain = attack_simple_ecb_oracle(simple_ecb_oracle(True))
    print(f"Decoded message:\n{plain}")
    print("\n")

    print("==================== Challenge 15 ====================")
    s0 = b"ICE ICE BABY\x04\x04\x04\x04"
    s1 = b"ICE ICE BABY\x04\x04\x04\x11"
    s2 = b"ICE ICE BABY\x03\x03\x03\x03"
    s3 = b"ICE ICE BABY\x05\x05\x05\x05"
    try:
        validate_padding(s0)
        print("s0 is valid")
    except PaddingError: pass
    try:
        validate_padding(s1)
        print("s1 is valid")
    except PaddingError: pass
    try:
        validate_padding(s2)
        print("s2 is valid")
    except PaddingError: pass
    try:
        validate_padding(s3)
        print("s3 is valid")
    except PaddingError: pass
    print("\n")

    print("==================== Challenge 16 ====================")
    CBC_attack = CBCBitflippingAttack()
    known = b'\0' * 32 # b"comment1=cooking%20MCs;userdata=" + b'\0' * 32 + b";comment2=%20like%20a%20pound%20of%20bacon"
    cip = CBC_attack.generate_query(known)
    target = xor(cip[32:48], b"\0"*16, b";admin=true;x=yy")
    result = cip[:32] + target + cip[48:]
    decip = CBC_attack.verify_admin(result)
    print(f"Is admin: {decip}")
    print("\n")
