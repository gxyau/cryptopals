#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../lib/base16.h"
#include "../lib/constants.h"
#include "../lib/utils.h"

int main() {
    // Challenge related
    uint8_t* stanza = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
    const uint8_t* KEY = "ICE";

    // XOR
    uint8_t* xor = calloc(strlen(stanza), sizeof(uint8_t));
    repeated_xor(stanza, KEY, xor, strlen(stanza), strlen(KEY));

    // Hex encoding
    size_t hex_len = base16_encode_len(strlen(xor));
    uint8_t* hex = calloc(hex_len, sizeof(uint8_t));
    base16_encode(xor, hex, strlen(xor));

    // Printing results
    printf("Hex: %s\n", hex);

    // Freeing memory
    free(xor);
    free(hex);

    // Exit successful
    return 0;
}
