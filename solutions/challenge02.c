#include "../lib/constants.h"
#include "../lib/utils.h"

int main() {
    uint8_t *s1  = "1c0111001f010100061a024b53535009181c";
    uint8_t *s2  = "686974207468652062756c6c277320657965";
    const int N  = strlen(s1);
    // Allocating memory for resulting xor string
    uint8_t* res = calloc(N, sizeof(uint8_t));
    printf("String length: %d\n", strlen(res));
    // Computing xor
    int status = str_xor(s1, s2, res, N);
    printf("String length: %d\n", strlen(res));
    if (status == -1) return EXIT_FAILURE;
    printf("The xor is: %s\n", res);
    // Exit succesful
    free(res);
    return 0;
}
