#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../lib/base16.h"
#include "../lib/constants.h"
#include "../lib/utils.h"

int main(int argc, char* argv[]) {
    // If no argument provided
    if (argc == 1) {
        printf("Please provide a file name.\n");
        exit(1); // From stdlib
    }
    const char* FILENAME = argv[1];
    FILE *fptr = fopen(FILENAME, "r");

    // Failed to open file
    if (fptr == NULL) {
        printf("Error: Failed to open file %s\n", FILENAME);
        exit(1);
    }

    // Reading lines and output xor
    uint8_t raw[62]; // Each line has 60 characters + null character + new line character
    uint8_t* best;
    double best_score = -1.0;
    const size_t DECODED_LEN = 30;
    int iter = 1, best_iter = 1;
    while(!feof(fptr)) {
        fgets(raw, 62, fptr); // Needs to include null character and new line character
        // Only wants the first 60 characters
        uint8_t b16[60];
        memcpy(b16, &raw[0], 60);
        b16[60] = '\0';
        printf("Line: %s\n", b16);

        // Decoding hex
        uint8_t* decoded = calloc(DECODED_LEN, sizeof(uint8_t));
        base16_decode(b16, decoded, strlen(b16));

        // Get the best candidate string
        uint8_t* plain = calloc(DECODED_LEN, sizeof(uint8_t));
        uint8_t key;
        double iter_score = guess_xor(decoded, plain, &key, DECODED_LEN);
        if (iter_score > best_score) {
            best_score = iter_score;
            best_iter = iter;
            best = plain;
        }

        // Printing best candidate string
        printf("Iteration %d best candidate: %s\n\n", iter++, plain);
        // Freeing memory
        free(decoded);
        free(plain);
    }

    // Printing overall best candidate
    printf("Overall best iteration: %d, with score: %f\n", best_iter, best_score);
    printf("Candidate: %s\n", best);

    // Closing the file and freeing memory
    fclose(fptr);

    // Exit successful
    return 0;
}

