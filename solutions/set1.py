from Crypto.Cipher import AES

def aes_ecb_decrypt(key, cipher):
    return AES.new(k, AES.MODE_ECB).decrypt(cipher)

def aes_ecb_encrypt(key, plain):
    return AES.new(k, AES.MODE_ECB).encrypt(plain)


if __name__ == "__main__":
    print("Cryptopals set 1\n")
    with open("c07.txt") as f:
        print(aes_ecb_decrypt(b"YELLOW SUBMARINE", f.read()))
