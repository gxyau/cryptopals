#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../lib/constants.h"
#include "../lib/base16.h"
#include "../lib/base64.h"

int main(int argc, char* argv[]) {
    char* hex_string = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    size_t hex_decoded_length = base16_decode_len(strlen(hex_string));
    uint8_t* plain = calloc(hex_decoded_length, sizeof(uint8_t));
    // Weird, declaring it here will run smoothly but if you declare it
    // later it will not run...
    size_t b64_encoded_length = base64_encode_len(hex_decoded_length);
    uint8_t* b64_string = calloc(b64_encoded_length, sizeof(uint8_t));
    // Fail to allocate memory
    if (!plain || !b64_string) return EXIT_FAILURE;

    int status = base16_decode((uint8_t*) hex_string, plain, strlen(hex_string));
    // Failed to decode
    if (status == -1) return EXIT_FAILURE;
    printf("Plain text is: %s\n", plain);

    // Base 64 encryption
    status = base64_encode((uint8_t*) plain, b64_string, strlen(plain));

    // Failed to encode
    if (status == -1) return EXIT_FAILURE;

    // Printing result
    printf("Base64 encoded string: %s\n", b64_string);

    // Freeing memory
    free(plain);
    free(b64_string);

    // Exit successful
    return 0;
}
