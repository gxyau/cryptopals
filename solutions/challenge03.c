#include "../lib/base16.h"
#include "../lib/constants.h"
#include "../lib/utils.h"

int main(int argc, char* argv[]) {
    // Decoding hex to ascii
    uint8_t* hex_encoded_string = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
    size_t decoded_length = base16_decode_len(strlen(hex_encoded_string));
    uint8_t* decoded = calloc(decoded_length, sizeof(uint8_t));
    int status = base16_decode(hex_encoded_string, decoded, strlen(hex_encoded_string));

    // Failed to decode
    if (status == -1) {
        printf("Failed to decode hex string.\n");
        return EXIT_FAILURE;
    }

    // Finding best score
    char guess;
    double max = -1.0;
    for (int i = 0; i < 256; ++i) {
        double current_score = score(decoded, i, decoded_length);
        if (current_score > max) {
            guess = i;
            max   = current_score;
        }
    }
    // Printing candidate
    printf("Candidate: %d, score: %f\n", guess, max);

    // getting xor string
    uint8_t* plain = calloc(decoded_length, sizeof(uint8_t));
    status = bit_xor(decoded, guess, plain, decoded_length);
    // Failed to xor
    if (status == -1) {
        printf("Failed to xor strings, check input length of strings.");
        return EXIT_FAILURE;
    }
    // Printing string
    printf("The string is: %s\n", plain);

    // Freeing memory
    free(decoded);
    free(plain);

    // Exit successful
    return 0;
}
