#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../lib/base64.h"
#include "../lib/constants.h"
#include "../lib/utils.h"

void printhex(const uint8_t* data, size_t ln) {
    while (ln--) {
        printf("%02x", *(data++));
    }
    printf("\n");
}

int main(int argc, char* argv[]) {
    // Ensuring the text file is included in the argument
    if (argc == 1) {
        printf("Please provide an argument\n");
        exit(1);
    }

    // Getting file names and file pointers
    const char* FILENAME = argv[1];
    FILE *fptr = fopen(FILENAME, "r");

    // Failed to open file
    if (fptr == NULL) {
        printf("Error: Failed to open file %s\n", FILENAME);
        exit(1);
    }

    // Readings lines and concatenation
    uint8_t raw[100];
    int index = 0, line = 0;
    uint8_t* encryption = calloc(5000, sizeof(uint8_t));
    while(fgets(raw, 100, fptr) != NULL) {
        int raw_len = strlen(raw);
        memcpy(encryption+index, raw, raw_len-1);
        index += raw_len-1; // last character is new line character
    }
    encryption[index] = '\0';
    fclose(fptr); // Closing file
    printf("The encryption is:\n%s\n", encryption);
    printf("Encryption length: %d, index: %d\n\n\n", strlen(encryption), index);

    // Base64 decoded
    size_t b64_decoded_len = base64_decode_len(strlen(encryption), 1);
    uint8_t* decoded = calloc(b64_decoded_len+1, sizeof(uint8_t));
    int status = base64_decode(encryption, decoded, strlen(encryption));
    if (status == -1) return EXIT_FAILURE;
    printf("B64 decoded length: %d\n\n\n", b64_decoded_len);

    // Guessing key length
    int keylen = guess_key_size(decoded, b64_decoded_len, 2, 40);
    if (keylen == -1) return EXIT_FAILURE;
    printf("Most probable key length is %d\n\n", keylen);

    // Juxtaposition and guessing key
    uint8_t* key = calloc(keylen+1, sizeof(uint8_t));
    for(int i = 0; i < keylen; ++i) {
        printf("Line i: %d\n", i);
        size_t block_len = (b64_decoded_len/keylen) + (i < (b64_decoded_len%keylen));
        uint8_t* block = calloc(block_len+1, sizeof(uint8_t));
        juxtapose(decoded, i, keylen, block, b64_decoded_len);
        printhex(block, block_len);
        uint8_t candidate = 0x00;
        guess_xor(block, block, &candidate, block_len);
        printf("Block: %s\n", block);
        key[i] = candidate;
        printf("Potential key: %c\n", candidate);
        free(block);
    }
    key[keylen] = '\0';
    printf("key: %s\n", key);

    // Repeated xor
    uint8_t* plain = calloc(b64_decoded_len+1, sizeof(uint8_t));
    repeated_xor(decoded, key, plain, b64_decoded_len, keylen);
    plain[b64_decoded_len] = '\0';
    printf("Plain text:\n%s\n", plain);

    // Clearing memory, just good practice
    free(encryption); // Clearing memory
    free(decoded);
    free(key);
    free(plain);

    // Exit successful
    return 0;
}
